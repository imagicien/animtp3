#include "TechniqueFactory.h"

#include <cstdlib>

#include "TechniqueToon.h"
#include "TechniqueBarla.h"
#include "TechniquePlainColor.h"
#include "TechniqueFixedPipeline.h"

RenderingTechnique* TechniqueFactory::CreateTechnique(Technique aTech)
{
    switch(aTech)
    {
    case Technique_ToonShader :
        return new TechniqueToon();
    case Technique_BarlaShader :
		return new TechniqueBarla();
    case Technique_PlainColor :
		return new TechniquePlainColor();
    case Technique_FixedPipeline :
		return new TechniqueFixedPipeline();
    case Technique_Twist :
        
    case Technique_MultiTexture :
        
    case Technique_Bonus :
    default:
        return NULL;
    }
}
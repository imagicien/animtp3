#include "RenderingTechnique.h"
#include "ShaderProgram.h"
#include <vector>

class TechniquePlainColor : public RenderingTechnique
{
public:
	TechniquePlainColor();
	virtual ~TechniquePlainColor();

	virtual void PreRender();
	virtual void PostRender();
	virtual void ReceiveInput(char aKey);
protected:

	void InitColors();
	void InitShader();
	
	GLfloat* mCurrentColor;
	std::vector<GLfloat*> mColors;
	ShaderProgram mShader;
};
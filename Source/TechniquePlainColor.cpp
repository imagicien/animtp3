#include "TechniquePlainColor.h"

TechniquePlainColor::TechniquePlainColor()
: mCurrentColor(0)
{
	InitColors();
	InitShader();
}

void TechniquePlainColor::PreRender()
{
	mShader.Activate();

	// Set the color in the shader program
	glUniform4f(glGetUniformLocation(mShader.ProgramId(), "Color"), mCurrentColor[0], mCurrentColor[1], mCurrentColor[2], mCurrentColor[3]);
}

void TechniquePlainColor::PostRender()
{
	mShader.Deactivate();
}

void TechniquePlainColor::ReceiveInput(char aKey)
{
	// Switch shader plain color
	switch (aKey)
	{
	case '1':
		mCurrentColor = mColors[0];
		break;
	case '2':
		mCurrentColor = mColors[1];
		break;
	case '3':
		mCurrentColor = mColors[2];
		break;
	case '4':
		mCurrentColor = mColors[3];
		break;
	case '5':
		mCurrentColor = mColors[4];
		break;
	default:
		break;
	}
}

void TechniquePlainColor::InitColors()
{
	// Create color presets
	mColors.push_back(new GLfloat[4]{ 1.0, 0.0, 0.0 }); //red  
	mColors.push_back(new GLfloat[4]{ 0.0, 1.0, 0.0 }); //green
	mColors.push_back(new GLfloat[4]{ 0.0, 0.0, 1.0 }); //blue 
	mColors.push_back(new GLfloat[4]{ 1.0, 1.0, 1.0 }); //white
	mColors.push_back(new GLfloat[4]{ 0.0, 0.0, 0.0 }); //black

	mCurrentColor = mColors[0];
}

void TechniquePlainColor::InitShader()
{
	//Create two shader objects.
	Shader* VertexShader = new Shader(Shader::Shader_Vertex);
	Shader* FragmentShader = new Shader(Shader::Shader_Fragment);

	VertexShader->CompileSourceFile("Resources/PlainColor.vx");
	FragmentShader->CompileSourceFile("Resources/PlainColor.fg");

	mShader.AddShader(VertexShader, true);
	mShader.AddShader(FragmentShader, true);

	//Link the program.
	mShader.Link();

}

TechniquePlainColor::~TechniquePlainColor()
{
}


#ifndef TECHNIQUEBARLA_H_
#define TECHNIQUEBARLA_H_

#include "RenderingTechnique.h"
#include "ShaderProgram.h"

class TechniqueBarla : public RenderingTechnique
{
public:

	TechniqueBarla();
	virtual ~TechniqueBarla();

	virtual void PreRender();
	virtual void PostRender();
	virtual void ReceiveInput(char aKey);

protected:

	void InitTextures();
	void InitShader();

	unsigned int mCurrentLookup;
	unsigned int mLookups[3];

	ShaderProgram mShader;
};

#endif //TECHNIQUEBARLA_H_

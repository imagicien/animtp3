#include "TechniqueFixedPipeline.h"
#include <iostream>

TechniqueFixedPipeline::TechniqueFixedPipeline()
{
	InitShader();
}

TechniqueFixedPipeline::~TechniqueFixedPipeline()
{
}

void TechniqueFixedPipeline::InitShader()
{
	Shader* VertexShader = new Shader(Shader::Shader_Vertex);
	Shader* FragmentShader = new Shader(Shader::Shader_Fragment);

	VertexShader->CompileSourceFile("Resources/FixedPipeline.vx");
	FragmentShader->CompileSourceFile("Resources/FixedPipeline.fg");

	mShader.AddShader(VertexShader, true);
	mShader.AddShader(FragmentShader, true);

	//Link the program.
	mShader.Link();
	 
	mShader.Link();
}

void TechniqueFixedPipeline::PreRender()
{
	mShader.Activate();
}

void TechniqueFixedPipeline::PostRender()
{
	mShader.Deactivate();
}

void TechniqueFixedPipeline::ReceiveInput(char aKey)
{
}
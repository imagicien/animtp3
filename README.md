# README #

```
Carl Lemaire      13 033 549
Mathieu Grondin   13 074 059
```

## Étapes pour que ça fonctionne: ##

1. Ajouter au dossier `Source` les dossiers `GLUT` et `Glew` trouvés dans le zip de Godin sous `Source`;
2. Copier `glut32.dll` et `glew32.dll` dans le même dossier que l'exécutable;
3. Ajouter au dossier `animtp3` le dossier `Resources` trouvé dans le zip de Godin.